import React, { useState } from "react";
import { styled, alpha } from "@mui/material/styles";
import {
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Typography,
  InputBase,
  Badge,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Fab,
  Drawer,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import LocationOnIcon from "@mui/icons-material/LocationOn";

// _____ Search Design _____
const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

// _____ Search Design Icon Wrapper_____
const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

// _____ Cart Wrapper for Alignment _____
const CartWrapper = styled("aside")({
  fontFamily: "Arial, Helvetica, sans-serif",
  width: "500px",
  padding: "20px",
});

// _____ Cart Item Wrapper for Alignment _____
const CartItemWrapper = styled("div")({
  display: "flex",
  justifyContent: "space-between",
  fontFamily: "Arial, Helvetica, sans-serif",
  borderBottom: "1px solid lightblue",
  paddingBottom: "20px",

  div: {
    flex: 1,
  },
});

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

const StyledFab = styled(Fab)({
  margin: "0 auto",
  height: "40px",
  width: "40px",
});

export default function Events({ events }) {

  const [cartItems, setCartItems] = useState([]);
  const [cartOpen, setCartOpen] = useState(false);
  const [filteredData, setFilteredData] = useState([]);

  //   _____ Add to Cart Function _____
  const handleAddToCart = (clickedItem) => {
    setCartItems((prev) => {
      const isItemInCart = prev.find((item) => item._id === clickedItem._id);

      if (isItemInCart) {
        return prev.map((item) =>
          item._id === clickedItem._id
            ? { ...item /* amount: item.amount + 1 */ }
            : item
        );
      }

      return [...prev, { ...clickedItem /* amount: 1 */ }];
    });
  };

  //   _____ Remove from Cart Function _____
  const handleRemoveFromCart = (id) => {
    setCartItems((prev) => prev.filter((item) => item._id !== id));
  };

  //   _____ Search Function _____
  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = events.filter((data) => {
      return data.title.search(value) !== -1;
    });
    setFilteredData(result);
  };

  return (
    <>
      {/* _____________ Navbar Starts _____________ */}
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="fixed">
          <Toolbar>
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Search…"
                inputProps={{ "aria-label": "search" }}
                onChange={(e) => handleSearch(e)}
              />
            </Search>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: "none", md: "flex" } }}>
              <IconButton size="large" aria-label="cart" color="inherit">
                <Badge
                  badgeContent={cartItems.length > 0 ? cartItems.length : null}
                  color="error"
                >
                  <ShoppingCartIcon onClick={() => setCartOpen(true)} />
                </Badge>
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
      </Box>
      {/* _____________ Navbar Ends _____________ */}

      {/* _______________ Cart Drawer Starts ___________________ */}
      <Drawer anchor="right" open={cartOpen} onClose={() => setCartOpen(false)}>
        <CartWrapper>
          <h2>Your Cart</h2>
          {cartItems.length === 0 ? <p>No items in cart.</p> : null}
          {cartItems.map((item) => (
            <CartItemWrapper key={item._id}>
              <div>
                <h3>{item.title}</h3>
                <div
                  className="information"
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <p>City: {item.city}</p>
                  <p>Date/Time: {item.date}</p>
                </div>

                <IconButton
                  size="medium"
                  aria-label="cart"
                  color="inherit"
                  style={{ float: "right" }}
                >
                  <DeleteIcon onClick={() => handleRemoveFromCart(item._id)} />
                </IconButton>
              </div>
              <img
                src={item.flyerFront}
                alt={item.title}
                style={{
                  maxWidth: "80px",
                  objectFit: "cover",
                  marginLeft: "40px",
                  paddingTop: "10px",
                }}
              />
            </CartItemWrapper>
          ))}
        </CartWrapper>
      </Drawer>
      {/* _______________ Cart Drawer Ends ___________________ */}

      {/* _______________ Cards Section Starts ____________________ */}
      <Container sx={{ my: 20 }} maxWidth="md">
        <Grid container spacing={4}>
          {events && events.length > 0 && filteredData.length > 0
            ? filteredData.map((event) => (
                <Grid item key={event._id} xs={12} sm={6} md={4}>
                  <Card
                    sx={{
                      height: "100%",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <CardMedia
                      component="img"
                      //   sx={{
                      //     // 16:9
                      //     pt: "16.25%",
                      //   }}
                      image={
                        event.flyerFront && event.flyerFront.length > 0
                          ? event.flyerFront
                          : "images/not-found.jpg"
                      }
                      alt="random"
                      height="400vh"
                    />
                    <CardContent sx={{ flexGrow: 1 }}>
                      <Typography gutterBottom variant="h5" component="h2">
                        <IconButton
                          color="primary"
                          aria-label="location"
                          style={{ marginLeft: "-17px" }}
                        >
                          <LocationOnIcon />
                        </IconButton>
                        <a
                          href={event.venue.direction}
                          style={{ textDecoration: "none", color: "black" }}
                        >
                          {event.venue.name}
                        </a>
                      </Typography>
                      <Typography>
                        <strong>Starts: </strong>
                        <span style={{ fontSize: "0.9rem" }}>
                          {event.startTime ? event.startTime : event.date}
                        </span>
                      </Typography>
                      <Typography>
                        <strong>Ends: </strong>
                        <span style={{ fontSize: "0.9rem" }}>
                          {event.endTime ? event.endTime : "N/A"}
                        </span>
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <StyledFab
                        color="primary"
                        aria-label="add"
                        style={{ float: "right" }}
                      >
                        <AddIcon onClick={() => handleAddToCart(event)} />
                      </StyledFab>
                      {/* <Button size="small">View</Button>
                    <Button size="small">Edit</Button> */}
                    </CardActions>
                  </Card>
                </Grid>
              ))
            : events &&
              events.length > 0 &&
              events
                .sort((a, b) =>
                  a.date
                    .split("-")
                    .reverse()
                    .join()
                    .localeCompare(b.date.split("-").reverse().join())
                )
                .map((event) => (
                  <Grid item key={event._id} xs={12} sm={6} md={4}>
                    <Card
                      sx={{
                        height: "100%",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <CardMedia
                        component="img"
                        //   sx={{
                        //     // 16:9
                        //     pt: "16.25%",
                        //   }}
                        image={
                          event.flyerFront && event.flyerFront.length > 0
                            ? event.flyerFront
                            : "images/not-found.jpg"
                        }
                        alt="random"
                        height="400vh"
                      />
                      <CardContent sx={{ flexGrow: 1 }}>
                        <Typography gutterBottom variant="h5" component="h2">
                          <IconButton
                            color="primary"
                            aria-label="location"
                            style={{ marginLeft: "-17px" }}
                          >
                            <LocationOnIcon />
                          </IconButton>
                          <a
                            href={event.venue.direction}
                            style={{ textDecoration: "none", color: "black" }}
                          >
                            {event.venue.name}
                          </a>
                        </Typography>
                        <Typography>
                          <strong>Starts: </strong>
                          <span style={{ fontSize: "0.9rem" }}>
                            {event.startTime ? event.startTime : event.date}
                          </span>
                        </Typography>
                        <Typography>
                          <strong>Ends: </strong>
                          <span style={{ fontSize: "0.9rem" }}>
                            {event.endTime ? event.endTime : "N/A"}
                          </span>
                        </Typography>
                      </CardContent>
                      <CardActions>
                        <StyledFab
                          color="primary"
                          aria-label="add"
                          style={{ float: "right" }}
                        >
                          <AddIcon onClick={() => handleAddToCart(event)} />
                        </StyledFab>
                        {/* <Button size="small">View</Button>
                    <Button size="small">Edit</Button> */}
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
        </Grid>
      </Container>
      {/* _______________ Cards Section Ends ____________________ */}
    </>
  );
}
