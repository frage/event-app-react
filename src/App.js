import Events from "./components/Events";
import React, { useEffect, useState } from "react";

function App() {
  const [events, setEvents] = useState(null);

  useEffect(() => {
    fetch("https://tlv-events-app.herokuapp.com/events/uk/london")
      .then((response) => response.json())
      .then((data) => setEvents(data));
  }, [fetch]);

  return (
    <>
      <Events events={events} />
    </>
  );
}

export default App;
